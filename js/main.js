'use strict';

let gameIsStarted,
    bricksArray,
    killedBricks = 0,
    lostState = 0;

function initialize() {
    let player1 = new Player(),
        ball = new Ball(player1),
        requestAnimationFrame;

    gameIsStarted = false;
    bricksArray = LevelsGenerator._createLevel1();

    document.addEventListener('keypress', function(event) {
        if (event.keyCode == 32) gameIsStarted = true;
        Util.adjustPictureForGame();
    });
    document.addEventListener('click', function() {
        gameIsStarted = true;
        Util.adjustPictureForGame();
    });

    requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    window.requestAnimationFrame = requestAnimationFrame;

    function render() {
        player1.move();
        ball.move();
        requestAnimationFrame(render);
    }

    requestAnimationFrame(render);
}

window.onload = initialize;
