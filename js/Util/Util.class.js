class Util {
    static rect2rectCollision(obj1, obj2) {
        return (obj1.left <= obj2.right
        && obj1.right  >=  obj2.left
        && obj1.bottom >=  obj2.top
        && obj1.top <= obj2.bottom);
    }

    static adjustPictureForGame() {
        document.body.style.cursor = 'none';
        document.querySelector('.overlay').style.display = 'none';
    }

    static gameOver() {
        let overlay = document.querySelector('.overlay');
        gameIsStarted = false;
        lostState++;
        document.body.style.cursor = '';
        overlay.querySelector('.overlay--text').innerHTML = `<span class="important-message">You lose</span><br>Click to continue<br>Click F5 to start again`;
        overlay.style.display = '';
    }

    static win() {
        let overlay = document.querySelector('.overlay');
        gameIsStarted = false;
        document.body.style.cursor = '';
        if (!lostState) overlay.querySelector('.overlay--text').innerHTML = `<span class="important-message">You win</span><br>For real<br>Click F5 to start again`;
        if (lostState) overlay.querySelector('.overlay--text').innerHTML = `<span class="important-message">You win</span><br>-ish<br>Click F5 to start again`;
        overlay.style.display = '';
    }
}