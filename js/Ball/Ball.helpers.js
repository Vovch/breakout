class MoveHelper {
    static moveWithBat(ball) {
        ball.leftPosition = parseFloat(ball.playerModel.style.left) * 0.01 * document.body.clientWidth;
        ball.topPosition = document.body.clientHeight - 50;
    }
    static moveFreely(ball) {
        ball.leftPosition = parseFloat(ball.ballModel.style.left);
        ball.topPosition = parseFloat(ball.ballModel.style.top);

        ball.leftPosition += ball.speedHorizontal;
        ball.topPosition += ball.speedVertical;
    }
    static getBallStats(ball) {
        let ballGettableProps,
            ballCalculatedProps;

        ballGettableProps = {
            ballHeight: ball.ballModel.offsetHeight,
            ballWidth: ball.ballModel.offsetWidth,
            ballLeft: ball.ballModel.offsetLeft,
            ballTop: ball.ballModel.offsetTop
        };
        ballCalculatedProps = {
            ballRight: ballGettableProps.ballLeft + ballGettableProps.ballWidth,
            ballDown: ballGettableProps.ballTop + ballGettableProps.ballHeight,
            ballCenterLeft: ballGettableProps.ballLeft + ballGettableProps.ballWidth / 2,
            ballCenterTop: ballGettableProps.ballTop + ballGettableProps.ballHeight / 2,
            ballCenterOffset: ballGettableProps.ballWidth * Math.sqrt(2) / 4
        };

        return Object.assign({}, ballGettableProps, ballCalculatedProps);
    }
}

class CollisionHelper {
    static checkCollisionsWithViewportEdges(ball) {
        let collidedWithEdges = '';

        if (ball.leftPosition < VIEWPORT_BORDER_RANGE) {
            ball.leftPosition = VIEWPORT_BORDER_RANGE;
            collidedWithEdges = COLLISIONS.VIEWPORT_LEFT;
            console.log('collided with viewport left');
        }
        if (ball.leftPosition > document.body.clientWidth - VIEWPORT_BORDER_RANGE) {
            ball.leftPosition = document.body.clientWidth - VIEWPORT_BORDER_RANGE;
            collidedWithEdges = COLLISIONS.VIEWPORT_RIGHT;
            console.log('collided with viewport right');
        }
        if (ball.topPosition < VIEWPORT_BORDER_RANGE) {
            ball.topPosition = VIEWPORT_BORDER_RANGE;
            collidedWithEdges = COLLISIONS.VIEWPORT_TOP;
            console.log('collided with viewport top');
        }
        if (ball.topPosition > document.body.clientHeight - VIEWPORT_BORDER_RANGE) {
            collidedWithEdges = COLLISIONS.VIEWPORT_BOTTOM;
            console.log('collided with viewport bottom');
        }

        return collidedWithEdges;
    }
    static processCollisionWithViewportEdges(collisionState, ball) {
        switch (collisionState) {
            case COLLISIONS.VIEWPORT_LEFT:
            case COLLISIONS.VIEWPORT_RIGHT:
                ball.speedHorizontal = -ball.speedHorizontal;
                break;
            case COLLISIONS.VIEWPORT_TOP:
                ball.speedVertical = -ball.speedVertical;
                break;
            case COLLISIONS.VIEWPORT_BOTTOM:
                Util.gameOver();
                ball._init();
                break;
            default:
                console.error('Error colliding with level edges');
                break;
        }
    }
    static isCollidedWithBat(ball, playerClientRect) {
        return ball.topPosition + BALL_SIZE >= playerClientRect.top
            && ball.leftPosition + VIEWPORT_BORDER_RANGE >= playerClientRect.left
            && ball.leftPosition - VIEWPORT_BORDER_RANGE <= playerClientRect.right;
    }
    static processCollisionWithBat(ball, playerClientRect) {
        let percentsOfPad = ((ball.leftPosition - (ball.playerModel.offsetLeft + ball.playerModel.offsetWidth / 2)) / ball.playerModel.offsetWidth) * 1.5;

        ball.speedHorizontal = percentsOfPad * ball.ballSpeed;
        ball.speedVertical = -(Math.sqrt(Math.pow(ball.ballSpeed, 2) - Math.pow(ball.speedHorizontal, 2)));
        ball.topPosition = playerClientRect.top - BALL_SIZE * 3;
        console.log('collided with the bat');
    }
    static processCollisionsWithBricks(ball) {
        let ballStats;

        ballStats = MoveHelper.getBallStats(ball);

        let ballVerticalDelta,
            ballHorizontalDelta,
            ballEquationTangens,
            ballEquationBPoint,
            ballCollisionPoints,
            isCollidedHorizontal = false,
            isCollidedVertical = false;

        let xMin = Math.min(ballStats.ballLeft, ball.oldLeft),
            xMax = Math.max(ballStats.ballLeft, ball.oldLeft),
            yMin = Math.min(ballStats.ballTop, ball.oldTop),
            yMax = Math.max(ballStats.ballTop, ball.oldTop);

        for (let i = 0; i < Util.positions.length; i++) {
            let brickCollisionsModel,
                brickModel;

            if (Util.positions[i].dead === true ) {
                continue;
            }

            brickCollisionsModel = Util.positions[i];
            brickModel = bricksArray[i].brickModel;

            ballVerticalDelta = ballStats.ballLeft - ball.oldLeft;
            ballHorizontalDelta = ballStats.ballTop - ball.oldTop;
            ballEquationTangens = ballVerticalDelta / ballHorizontalDelta;
            ballEquationBPoint = ball.oldTop - ballEquationTangens * ball.oldLeft;

            ballCollisionPoints = [
                {
                    x: brickCollisionsModel.left,
                    y: ballEquationTangens * brickCollisionsModel.left + ballEquationBPoint,
                    where: 'left'
                },
                {
                    x: brickCollisionsModel.right,
                    y: ballEquationTangens * brickCollisionsModel.right + ballEquationBPoint,
                    where: 'right'
                },
                {
                    x: (brickCollisionsModel.top - ballEquationBPoint) / ballEquationTangens,
                    y: brickCollisionsModel.top,
                    where: 'top'
                },
                {
                    x: (brickCollisionsModel.bottom - ballEquationBPoint) / ballEquationTangens,
                    y: brickCollisionsModel.bottom,
                    where: 'bottom'
                }
            ];
            ballCollisionPoints = ballCollisionPoints.filter(function getPointsInsideMoveRadius(point) {
                // TODO: If the line that connects old and new ball center intersects with brick walls OR
                // TODO: the point is in radius of the new ball position...
                // Get four lines, one for each corner?
                return Util.rect2rectCollision(
                    {
                        left: xMin,
                        right: xMax,
                        top: yMin,
                        bottom: yMax
                    },
                    brickCollisionsModel) &&
                    Util.rect2rectCollision({
                        left: point.x,
                        right: point.x,
                        top: point.y,
                        bottom: point.y
                    },
                    brickCollisionsModel);
            });

            // Leave only one point, which is the closest to old position.
            let lengths = [];
            ballCollisionPoints.forEach(function getLengths(point) {
                point.length = Math.round(Math.sqrt(Math.pow(point.x - ball.oldLeft, 2) + Math.pow(point.y - ball.oldTop, 2)));
                lengths.push(point.length);
            });
            let minLength = Math.min.apply(this, lengths);
            ballCollisionPoints = ballCollisionPoints.filter(function selectClosestCollisions(point) {
                return point.length === minLength;
            });

            // TODO: Move the ball outside the block. Use only first point
            ballCollisionPoints.forEach(function processCollision(point) {
                console.log('collided with the brick');
                if (!isCollidedVertical) {
                    if ((point.where === 'top' || point.where === 'bottom')) {
                        ball.speedVertical = -ball.speedVertical;
                        ball.topPosition += ball.speedVertical * 2;
                        isCollidedVertical = true;
                        killBrick();
                    }
                }
                if (!isCollidedHorizontal) {
                    if (!(point.where === 'top' || point.where === 'bottom')) {
                        ball.speedHorizontal = -ball.speedHorizontal;
                        ball.leftPosition += ball.speedHorizontal * 2;
                        isCollidedHorizontal = true;
                        killBrick();
                    }
                }
            });
            function killBrick() {
                brickModel.style.display = 'none';
                Util.positions[i].dead = true;
                killedBricks++;
                if (killedBricks >= bricksArray.length) {
                    Util.win();
                }
            }
        }
    }
}