class Ball {
    constructor(associatedPlayer, ballSpeed) {
        this._createModel();
        this.playerModel = associatedPlayer.getModel();
        this._init(ballSpeed);
    }

    _init(ballSpeed) {
        this.ballSpeed = ballSpeed || 10;
        this.speedVertical = -this.ballSpeed / Math.sqrt(2);
        this.speedHorizontal = this.ballSpeed / Math.sqrt(2);
    }

    _createModel() {
        this.ballModel = document.createElement('div');

        this.ballModel.style.cssText = `
            width: ${BALL_SIZE}px;
            height: ${BALL_SIZE}px;
            position: absolute;
            bottom: 50px;
            margin-left: -${BALL_SIZE / 2}px;
            margin-top: -${BALL_SIZE / 2}px;
            background-color: #333;
            border-radius: 50%;
            transform: translateZ(0);
            -webkit-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
            -moz-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
            box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
        `;

        document.body.appendChild(this.ballModel);

        return this.ballModel;
    }

    move() {
        let viewportCollisionState,
            playerClientRect,
            self;

        self = this;

        playerClientRect = this.playerModel.getBoundingClientRect();
        if(!gameIsStarted) {
            MoveHelper.moveWithBat(self);
        }
        else {
            MoveHelper.moveFreely(self);
            viewportCollisionState = CollisionHelper.checkCollisionsWithViewportEdges(self);
            if (viewportCollisionState) {
                CollisionHelper.processCollisionWithViewportEdges(viewportCollisionState, self);
                return;
            }
            if (CollisionHelper.isCollidedWithBat(self, playerClientRect)) {
                CollisionHelper.processCollisionWithBat(self, playerClientRect);
                return;
            }

            CollisionHelper.processCollisionsWithBricks(self);
        }


        self.oldLeft = self.leftPosition;
        self.oldTop = self.ballModel.offsetTop;
        this.ballModel.style.left = this.leftPosition + 'px';
        this.ballModel.style.top = this.topPosition + 'px';
    }

    getModel() {
        return this.ballModel;
    }
}