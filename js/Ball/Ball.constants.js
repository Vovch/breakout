const COLLISIONS = {
          VIEWPORT_LEFT: 'collidedLeft',
          VIEWPORT_RIGHT: 'collidedRight',
          VIEWPORT_TOP: 'collidedTop',
          VIEWPORT_BOTTOM: 'collidedBottom',
      },
      VIEWPORT_BORDER_RANGE = 10,
      BALL_SIZE = 5; // In px