class Brick {
    constructor(x, y) {
        this._createModel(x, y);
    }

    _createModel(x, y) {
        this.brickModel = document.createElement('div');
        this.brickModel.style.cssText = "\
            width: 3%;\
            height: 2%;\
            position: absolute;\
            margin-left: -10px;\
            background-color: #333;\
            -webkit-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);\
            -moz-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);\
            box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);\
        ";
        this.brickModel.style.left =  x;
        this.brickModel.style.top =  y;

        document.body.appendChild(this.brickModel);

        return this.brickModel;
    }
}