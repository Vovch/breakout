class Player {
    constructor() {
        this._createModel();
        this.moveInit();
        this.mousePosition = document.body.clientWidth / 2;
    }

    _createModel() {
        this.playerModel = document.createElement('div');

        this.playerModel.style.cssText = `
            width: 10%;
            height: 2%;
            position: absolute;
            bottom: 20px;
            left: 50%;
            margin-left: -5%;
            transform: translateZ(0);
            background-color: #333;
            -webkit-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
            -moz-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
            box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
        `;

        document.body.appendChild(this.playerModel);

        return this.playerModel;
    }

    moveInit() {
        let moveSpeed = 1;
        let moveKeyNumbers = [37, 65, 39, 68]; // Left, left, right, right
        let self = this;

        self.currentSpeed   = 0;
        document.addEventListener('keydown', function(event) {
            let key = event.keyCode;

            if (key == moveKeyNumbers[0] || key == moveKeyNumbers[1]) {
                self.moveLeft = moveSpeed;
            } else if (key == moveKeyNumbers[2] || key == moveKeyNumbers[3]) {
                self.moveRight = moveSpeed;
            }
        });
        document.addEventListener('keyup', function(event) {
            let key = event.keyCode;

            if (key == moveKeyNumbers[0] || key == moveKeyNumbers[1]) {
                self.moveLeft = 0;
            } else if (key == moveKeyNumbers[2] || key == moveKeyNumbers[3]) {
                self.moveRight = 0;
            }
        });

        // TODO: rebuild with movement amount instead of pageX (is it possible for mouse to always stay inside?)
        document.addEventListener('mousemove', function(event) {
            self.mousePosition = event.pageX;
        });
    }


    move() {
        let maxRightPosition,
            newPosition;

        maxRightPosition = 95;
        newPosition = this.mousePosition / document.body.clientWidth * 100;

        // Limit pad movement
        if (newPosition < 5) newPosition = 5;
        if (newPosition > maxRightPosition)
            newPosition = maxRightPosition;

        // Move
        this.playerModel.style.left = newPosition + '%';
    }

    getModel() {
        return this.playerModel;
    }
}