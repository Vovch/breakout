class LevelsGenerator {
    static _createLevel1() {
        this.bricksArray = [];
        let x = 0, y = '5%';
        let i = 0;
        let maxY = 35;
        while (parseFloat(y) <= maxY) {
            x = parseFloat(x) + 4 + '%';
            if (parseFloat(x) > 95) {
                x = '4%';
                y = parseFloat(y) + 3 + '%';
            }
            if (parseFloat(y) >= maxY) break;
            this.bricksArray[i] = new Brick(x, y);
            i++;
        }

        // Create brick positions array
        Util.positions = [];
        let bricksArray = this.bricksArray;
        for (let i = 0; i < bricksArray.length; i++) {
            let brickModel = bricksArray[i].brickModel;
            let brickModelLeft = brickModel.offsetLeft;
            let brickModelRight = brickModelLeft + brickModel.offsetWidth;
            let brickModelTop = brickModel.offsetTop;
            let brickModelBottom = brickModelTop + brickModel.offsetHeight;

            Util.positions.push({
                left: brickModelLeft,
                right: brickModelRight,
                top: brickModelTop,
                bottom: brickModelBottom,
                dead: false
            });
        }

        return this.bricksArray;
    }
}